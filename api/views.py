from googleapiclient.discovery import build
from googleapiclient.http import MediaInMemoryUpload
from rest_framework import  viewsets
from django.http import HttpResponse
from rest_framework.decorators import action
from rest_framework.response import Response
from django.shortcuts import render
from driveapp.settings import GOOGLE_DRIVE_CREDENTIALS
from google.oauth2 import credentials
from rest_framework import status as drf_status
from google.oauth2 import service_account
from .models import UserProfile
from .serializers import UserProfileSerializer


class UserProfileViewSet(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer

    def create(self, request, *args, **kwargs):
        return render(request, 'upload_form.html')  

    @action(detail=False, methods=['post', 'get'], url_path='upload-photo', url_name='upload_photo')
    def upload_photo(self, request):
        if request.method == 'POST':
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            user_profile = serializer.save()
            
            credentials = service_account.Credentials.from_service_account_file(GOOGLE_DRIVE_CREDENTIALS)
            drive_service = build('drive', 'v3', credentials = credentials)
            file_obj = request.data['profile_photo']
            file_content = file_obj.read()
            file_metadata = {
                'name': f'{user_profile.id}_{user_profile.name}_profile_photo.jpg',
                'parents': ['1uKowMxgrKnDeZuJrK8Z3sRd-eHulu5zv']  
            }
            media = MediaInMemoryUpload(file_content, mimetype='image/jpeg')
            file = drive_service.files().create(body=file_metadata, media_body=media, fields='id').execute()
            user_profile.drive_file_id = file.get('id')  
            user_profile.save()
            return render(request, 'upload_success.html')
        elif request.method == 'GET':
            return render(request, 'upload_form.html')  
        return Response(status=drf_status.HTTP_405_METHOD_NOT_ALLOWED)  

    @action(detail=False, methods=['get'], url_path='get-photo/(?P<file_id>[^/.]+)/(?P<file_name>[^/.]+)', url_name='get_photo')
    def get_photo(self, request, file_id, file_name):
        credentials = service_account.Credentials.from_service_account_file(GOOGLE_DRIVE_CREDENTIALS)
        drive_service = build('drive', 'v3', credentials = credentials)
        desired_file_name = f'{file_id}_{file_name}' + '_profile_photo.jpg'
        print(desired_file_name)
        results = drive_service.files().list(q=f"name='{desired_file_name}'", fields='files(id, webContentLink)').execute()
        files = results.get('files', [])
        if len(files) == 0:
            return Response('File not found', status=drf_status.HTTP_404_NOT_FOUND)
        file = files[0]  
        file_id = file.get('id')
        web_content_link = file.get('webContentLink')
        max_width = 200 
        max_height = 200  
        image_html = f"<img src='{web_content_link}' alt='{file_name}' style='max-width: {max_width}px; max-height: {max_height}px;'>"
        link_html = f"<p>Google Drive link: <a href='{web_content_link}' target='_blank'>{web_content_link}</a></p>"
        html_content = f"{image_html}\n{link_html}"
        return HttpResponse(html_content)
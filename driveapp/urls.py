from django.urls import path, include
from rest_framework.routers import DefaultRouter
from api.views import UserProfileViewSet

router = DefaultRouter()
router.register(r'user-profile', UserProfileViewSet, basename='user-profile')

urlpatterns = [
    path('', include(router.urls)),
]

